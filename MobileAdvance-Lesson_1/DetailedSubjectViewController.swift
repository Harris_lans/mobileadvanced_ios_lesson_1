//
//  DetailedSubjectViewController.swift
//  MobileAdvance-Lesson_1
//
//  Created by S HARISH KUMAR on 09/07/18.
//  Copyright © 2018 Harish Kumar. All rights reserved.
//

import UIKit

class DetailedSubjectViewController: ViewController
{

    @IBOutlet weak var subjectLabelView: UILabel!
    var subjectString : String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        subjectLabelView.text = subjectString
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
