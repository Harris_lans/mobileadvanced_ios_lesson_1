//
//  CollectionViewController.swift
//  MobileAdvance-Lesson_1
//
//  Created by S HARISH KUMAR on 10/07/18.
//  Copyright © 2018 Harish Kumar. All rights reserved.
//

import UIKit

class CollectionViewController: UIViewController
{
    let emojis = ["🦈", "🐊", "🦖", "🐺", "🦕"];
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        myCollectionView.register(UINib(nibName:"CollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "CustomCollectionCell")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension CollectionViewController : UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let size = CGSize(width: 100, height: 150)
        return size
    }
}

extension CollectionViewController : UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return emojis.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionCell", for: indexPath) as! CollectionViewCell
        let emoji = emojis[indexPath.row]
        cell.emojiLabel.text = emoji
        return cell
    }
}
