//
//  TableViewCell.swift
//  MobileAdvance-Lesson_1
//
//  Created by S HARISH KUMAR on 09/07/18.
//  Copyright © 2018 Harish Kumar. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell
{
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
