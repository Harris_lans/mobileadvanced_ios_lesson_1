//
//  TableViewController.swift
//  MobileAdvance-Lesson_1
//
//  Created by S HARISH KUMAR on 09/07/18.
//  Copyright © 2018 Harish Kumar. All rights reserved.
//

import UIKit

class TableViewController: UIViewController
{

    @IBOutlet weak var myTableView: UITableView!
    
    // Data Members
    let subjects = ["iOS", "C++", "Unreal Engine", "Unity", "Cloud Development"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "CustomTableCell")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "DetailedSubjectView" {
            if let destinationController = segue.destination as?
                DetailedSubjectViewController {
                if let selectedRow = myTableView.indexPathForSelectedRow {
                    destinationController.subjectString = subjects[selectedRow.row]
                }
            }
        }
    }


}

extension TableViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        performSegue(withIdentifier: "DetailedSubjectView", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 300
    }
}

extension TableViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return subjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableCell", for: indexPath) as! TableViewCell
        let subject = subjects[indexPath.row]
        cell.titleLabel.text = subject
        return cell
    }
    
    
}
